# Scenarios

## Frontend testing

Check system behaviour when the application sends request is working as expected

Expected: headers are added to each request

## Server testing

Check system behaviour when user reaches main page is working as expected.

Expected: return angular application.

## Integration testing

Check system behaviour when API creates a new task in the database is working as expected.

Expected: return oid of the new task

